from extractor import DataExtractor
from analyser import DataAnalyser


class Football:
    def __init__(self, file):
        self.file = file
        self.extractor = DataExtractor(self.file)
        self.fb_analyser = DataAnalyser()

    def min_f_a_diff(self):
        data = self.extractor.extract_data()
        return self.fb_analyser.analyser(data, "A", "F", "Team")


if __name__ == '__main__':
    f1 = Football("football.dat")
    print(f1.min_f_a_diff())

"""
Spike implementation
"""
# class Football:
#     def __init__(self, file):
#         self.file = file

#     def column_diff(self):
#         with open(self.file) as f:
#             next(f)
#             team =""
#             min_diff = float('inf')
#             for line in f:
#                 row = line.split()
#                 print(row)
#                 try:
#                     diff = abs(int(row[8]) - int(row[6]))
#                 except (ValueError, IndexError):
#                     continue
#                 if diff < min_diff:
#                     team = row[1]
#                     min_diff = diff
#             print(team)
