class DataExtractor:
    def __init__(self, file):
        self.file = file

    def extract_data(self):
        with open(self.file) as f:
            header = []
            first_line = f.readline()
            if first_line[:3] == "   ":
                header.append("missed_column")
            header = header + first_line.split()
            data = []
            for line in f:
                line = line.replace("-", "")
                row = line.split()
                d = {}
                for i in range(len(row)):
                    d[header[i]] = row[i]
                if len(d) == 0:
                    continue
                data.append(d)
        return data
