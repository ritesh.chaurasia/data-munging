from extractor import DataExtractor
from analyser import DataAnalyser


class Weather:
    def __init__(self, file):
        self.file = file
        self.extractor = DataExtractor(self.file)
        self.temp_analyser = DataAnalyser()

    def min_temp_spread(self):
        data = self.extractor.extract_data()
        return self.temp_analyser.analyser(data, "MxT", "MnT", "Dy")


if __name__ == '__main__':
    w1 = Weather("weather.dat")
    print(w1.min_temp_spread())


# class weather:
#     def __init__(self, file):
#         self.file = file

#     def temp_spread(self):
#         with open(self.file) as f:
#             next(f)
#             next(f)
#             day = 0
#             spread = 300
#             for line in f:
#                 row=line.split()
#                 try:
#                     diff=int(row[1]) - int(row[2])
#                 except ValueError:
#                     continue
#                 if diff < spread:
#                     day=row[0]
#                     spread=diff
#             print(day)

# w1=weather("weather.dat")
# w1.temp_spread()
