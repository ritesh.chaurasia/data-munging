class DataAnalyser:
    def analyser(self, data, col1, col2, output):
        m_diff = float('inf')
        for row in data:
            try:
                diff = abs(int(row[col1]) - int(row[col2]))
            except (ValueError, IndexError):
                continue
            if diff < m_diff:
                team = row[output]
                m_diff = diff
        return team
