# DATA MUNGING PROJECT

In this Project we will try to understand SOLID principles by doing Data Analysis on [weather data](https://gitlab.com/ritesh.chaurasia/data-munging/blob/master/weather.dat) and [football data](https://gitlab.com/ritesh.chaurasia/data-munging/blob/master/football.dat) <br>


## Getting Started

Download [weather data](https://gitlab.com/ritesh.chaurasia/data-munging/blob/master/weather.dat) and [football data](https://gitlab.com/ritesh.chaurasia/data-munging/blob/master/football.dat) from [katka code](http://codekata.com/kata/kata04-data-munging/) and read the instructions to implement the project

## Implementation Details

[Helper odule](https://gitlab.com/ritesh.chaurasia/data-munging/blob/master/helper.py) : It contains common classes that is to be used in both to extract and analyse data<br>

[football.py](https://gitlab.com/ritesh.chaurasia/data-munging/blob/master/football.py) and [temp_spread.py](https://gitlab.com/ritesh.chaurasia/data-munging/blob/master/temp_spread.py) contains the data analysis implementation of weather and football data